﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpriteGridSlicerWindow : EditorWindow
{
    private Texture2D m_texture;
    private int m_columns = 10;
    private int m_rows = 10;
    private Vector2 m_pivot = new Vector2(.5f, .5f);

    [MenuItem("Window/SpriteSlicer/Grid")]
    public static void ShowWindow()
    {
        GetWindow(typeof(SpriteGridSlicerWindow));
    }

    private void OnGUI()
    {
        m_texture = EditorGUILayout.ObjectField("Sprite: ", m_texture, typeof(Texture2D), false, GUILayout.MaxWidth(350)) as Texture2D;
        m_columns = EditorGUILayout.IntField("Columns: ", m_columns, GUILayout.MaxWidth(350));
        m_rows = EditorGUILayout.IntField("Rows: ", m_rows, GUILayout.MaxWidth(350));
        m_pivot = EditorGUILayout.Vector2Field("Pivot: ", m_pivot, GUILayout.MaxWidth(350));

        if (GUILayout.Button("Slice", GUILayout.MaxWidth(350)))
        {
            if (m_texture == null)
            {
                Debug.Log("Please select a texture");
                return;
            }

            var path = AssetDatabase.GetAssetPath(m_texture);
            var ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = true;

            ClearTextureImporter(path, ti);
            SliceSprites(path, ti);
        }
    }

    private static void ClearTextureImporter(string path, TextureImporter ti)
    {
        ti.spriteImportMode = SpriteImportMode.Single;
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }

    private void SliceSprites(string path, TextureImporter ti)
    {
        var newData = new List<SpriteMetaData>();
        ti.spriteImportMode = SpriteImportMode.Multiple;
        var xStep = m_texture.width / m_columns;
        var yStep = m_texture.height / m_rows;

        for (int y = 0; y < m_rows; y++)
        {
            for (int x = 0; x < m_columns; x++)
            {
                var i = x + (y * m_columns);
                Debug.Log(i);
                var smd = new SpriteMetaData();
                smd.pivot = m_pivot;
                smd.alignment = 9;
                smd.name = m_texture.name + "_" + i;
                smd.rect = new Rect(x * xStep, y * yStep, xStep, yStep);
                newData.Add(smd);
            }
        }

        ti.spritesheet = newData.ToArray();
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }
}
