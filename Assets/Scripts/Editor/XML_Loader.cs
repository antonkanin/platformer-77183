﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class XML_Loader
{
	[MenuItem("Tools/Create Sprites From Selection")]
	public static void LoadXML()
	{
		var selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

		foreach (var item in selection)
		{
			//Original path example: Assets/2D/Knight/knight_idle.png
			var imagePath = AssetDatabase.GetAssetPath(item);
			
			//Remove "Assets/"  and ".png" from path
			var xmlPath = imagePath.Replace("Assets/", "").Replace(".png", ""); 
			xmlPath = Path.Combine(Application.dataPath, xmlPath) + ".xml";

			SetSprites(imagePath, xmlPath);
		}
	}

	private static Texture2D LoadTexture(string filePath)
	{
		if (File.Exists(filePath))
		{
			var fileData = File.ReadAllBytes(filePath);
			var tex2D = new Texture2D(1, 1); // Create new "empty" texture
			if (tex2D.LoadImage(fileData)) return tex2D; // If data = readable -> return texture
		}

		return null; // Return null if load failed
	}

	private static void SetSprites(string imagePath, string xmlPath)
	{
		if (!File.Exists(imagePath))
		{
			Debug.LogError("Image not founded");
			return;
		}

		if (!File.Exists(xmlPath))
		{
			Debug.LogError("XML file not founded");
			return;
		}

		var texture2D = LoadTexture(imagePath);
		if (texture2D == null)
		{
			Debug.LogError("File is not an Image");
			return;
		}

		//Load and read Xml file
		var xmlDoc = new XmlDocument();
		xmlDoc.Load(xmlPath);

		//Create TextureAtlasContainer from XML file by parsing it
		var container = new TextureAtlasContainer();
		foreach (XmlNode node in xmlDoc.GetElementsByTagName("SubTexture"))
		{
			var subTexture = new SubTexture();
			subTexture.AddAttributes(node.Attributes);
			container.SubTextures.Add(subTexture);
		}

		//Load TextureImporter
		var ti = AssetImporter.GetAtPath(imagePath) as TextureImporter;
		if (ti == null)
		{
			return;
		}
		
		//Write all data from TextureAtlasContainer to TextureImporter
		ti.isReadable = true;
		ti.spriteImportMode = SpriteImportMode.Multiple;
		var newData = new List<SpriteMetaData>();
		
		foreach (var subText in container.SubTextures)
		{
			var smd = new SpriteMetaData();
			var x = (subText.Width * 0.5f + subText.FrameX) / subText.Width;
			var y = 1 - (subText.Height * 0.5f + subText.FrameY) / subText.Height;

            //Pivot is normalized value
            //In Unity Pivot (0, 0) is bottom-left corner
            smd.pivot = new Vector2(x, y);

            //Center = 0, TopLeft = 1, TopCenter = 2, TopRight = 3, LeftCenter = 4, RightCenter = 5,
            //BottomLeft = 6, BottomCenter = 7, BottomRight = 8, Custom = 9
            //Explanation: https://docs.unity3d.com/ScriptReference/SpriteMetaData-alignment.html
            smd.alignment = 9;
            smd.name = subText.Name;
			smd.rect = new Rect(subText.X, texture2D.height - subText.Y - subText.Height, subText.Width,
				subText.Height);
			newData.Add(smd);
		}

		//Store data on disk
		ti.spritesheet = newData.ToArray();
		AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate);
	}

	public class TextureAtlasContainer
	{
		public List<SubTexture> SubTextures = new List<SubTexture>();
	}

	public class SubTexture
	{
		public string Name = "";
		public int FrameHeight = 0;
		public int FrameWidth = 0;
		public int FrameX = 0;
		public int FrameY = 0;
		public int Height = 0;
		public int Width = 0;
		public int X = 0;
		public int Y = 0;

		public void AddAttributes(XmlAttributeCollection attributes)
		{
			foreach (XmlAttribute attribute in attributes) AddAttribute(attribute);
		}

		public void AddAttribute(XmlAttribute attribute)
		{
			AddAttribute(attribute.Name, attribute.Value);
		}

		public void AddAttribute(string atrName, string atrValue)
		{
			var fields = GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

			foreach (var field in fields)
			{
				if (string.Equals(atrName, "name", StringComparison.CurrentCultureIgnoreCase))
				{
					Name = atrValue;
					continue;
				}

				if (!string.Equals(field.Name, atrName, StringComparison.CurrentCultureIgnoreCase))
				{
					continue;
				}
				
				var val = 0;
				int.TryParse(atrValue, out val);

				field.SetValue(this, val);
			}
		}
	}
}